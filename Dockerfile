FROM openjdk:11-jre-slim
COPY target/microservice-istio-order-1.0.0.jar .
CMD java -Xmx300m -Xms300m -XX:TieredStopAtLevel=1 -noverify -jar microservice-istio-order-1.0.0.jar
EXPOSE 8080
